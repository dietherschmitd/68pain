unit datenbank;

interface

uses
  IBConnection;


implementation
var
  AConnection : TIBConnection;
function CreateConnection: TIBConnection;
begin
  result := TIBConnection.Create(nil);
  result.Hostname := 'localhost';
  //result.DatabaseName := '/opt/firebird/examples/employee.fdb';
  result.UserName := 'martin';
  result.Password := '123';
end;



begin
  AConnection := CreateConnection;
  AConnection.Open;
  if Aconnection.Connected then
    writeln('Successful connect!')
  else
    writeln('This is not possible, because if the connection failed, ' +
            'an exception should be raised, so this code would not ' +
            'be executed');
  AConnection.Close;
  AConnection.Free;
end.
